package com.yedam.app.user.mapper;

import java.util.List;

import com.yedam.app.user.RoleVO;
import com.yedam.app.user.UserVO;

public interface UserMapper {
	UserVO getUser(String id);
	List<RoleVO> getRole(String id);
}
