package com.yedam.app.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.yedam.app.user.UserVO;
import com.yedam.app.user.mapper.UserMapper;
import com.yedam.app.user.service.UserService;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	UserMapper userMapper;

	@Override
	public UserVO getUser(String id) {
		return userMapper.getUser(id);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserVO vo = userMapper.getUser(username);
		if(vo == null) {
			throw new UsernameNotFoundException("no user");
		}
		vo.setRoles(userMapper.getRole(username));
		return vo;
	}
}