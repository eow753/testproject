package com.yedam.app;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yedam.app.user.UserVO;

@Controller
public class BoardController {
	@RequestMapping
	public String list(@AuthenticationPrincipal UserVO vo) {
		return "board";
	}
}
