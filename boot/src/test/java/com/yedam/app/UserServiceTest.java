package com.yedam.app;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.yedam.app.user.UserVO;
import com.yedam.app.user.service.UserService;

@SpringBootTest
public class UserServiceTest {
	@Autowired UserService userService;
	@Test
	public void test() {
		UserVO vo = userService.getUser("user");
		assertNotNull(vo);
	}
}
