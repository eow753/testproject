package com.yedam.app;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncodingTest {
	@Test
	public void test() {
		BCryptPasswordEncoder bvrypt = new BCryptPasswordEncoder(16);
		String str = bvrypt.encode("2222");
		boolean result = bvrypt.matches("2222", str);
		System.out.println(str);
		System.out.println(result);
	}
}
